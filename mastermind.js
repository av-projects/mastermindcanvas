/**************************************************************
 * [config]: Configuration for visual interface and game setup
 * [render]: Rendering functions for drawing on canvas context
 * [input]: Functions used by event on DOM
 * [util]: Helper functions to manipulate the game
 **************************************************************/

// initialize canvas
var wrapper = document.getElementById('mastermind'),
	canvas = document.createElement('canvas'),
	ui = document.createElement('div');
	resetBtn = document.createElement('button'),
	checkBtn = document.createElement('button');

ui.setAttribute('class', 'ui');

resetBtn.innerText = "Reset Game";
resetBtn.setAttribute('class', 'reset-btn');

checkBtn.innerText = "Check";
checkBtn.setAttribute('class', 'check-btn');

canvas.width = board.canvas.w;
canvas.height = board.canvas.h;
wrapper.appendChild(canvas);
wrapper.appendChild(ui);
ui.appendChild(resetBtn);
ui.appendChild(checkBtn);

// get renderer
var ctx = canvas.getContext('2d');

// game elements
var dotCollection = [],
	hintCollection = [],
	poolCollection = [],
	hiddenCollection = [];

// packing collection references
var game = {
	// variables for mouse interaction
	lastPosition: null,
	poolIndex: -1,
	circleIndex: -1,
	attempts: board.rows-1,
	// [optional feature] can be externally loaded via xmlhttprequest
	colorCollection: colors,
	config: board,
	over: false,
	// getting displayed via the rendering scope
	renderObjects: []
};

initialize(game);
game.renderObjects = [].concat(dotCollection, hintCollection, poolCollection, hiddenCollection);
render(ctx, game);

// initiate animation loop
animationLoop(ctx, game);

// bind events
canvas.onmousedown = onMouseDown.bind(this, game);
canvas.onmousemove = onMouseMove.bind(this, ctx, game);
document.onmouseup = onMouseUp.bind(this, ctx, game);
resetBtn.onclick = reset.bind(this, ctx, game);
checkBtn.onclick = checkBoard.bind(this, ctx, game);

// helper functions
function initialize(game) {
	var board = game.config;

	game.poolIndex = -1;
	game.circleIndex = -1;
	game.attempts = board.rows-1;

	// draw dot area
	var dot, hints;
	for(var y = 0; y < board.rows; y++) {
		for(var x = 0; x < board.cols; x++) {
			ctx.fillStyle = board.dot.color.default;
			dot = createDot( 
				board.padding.left + board.x + x * board.spacing,
				board.y + board.padding.top + board.spacing * y,
				board.dot.radius, board.dot.color.default, y);
			dotCollection.push(dot);
		}

		// draw hints on the same row
		ctx.fillStyle = board.hint.color.default;
		hints = createHints(dot.x, dot.y, board.hint.radius, board.hint.color.default, 
			y, board.hint.offset);
		for(var i in hints) {
			hintCollection.push(hints[i]);
		}
	}

	// draw color pool
	var i = 0, color, coloredDot;
	for(var c in game.colorCollection) {
		color = game.colorCollection[c];

		ctx.fillStyle = color;
		coloredDot = createDot(
			board.padding.left + board.x + i + dot.radius*board.pool.spacing*i,
			dot.y+board.pool.offset.top,
			dot.radius, color, -1);
		poolCollection.push(coloredDot);
		i++;
	}

	// fill the hidden collection
	var hidden;
	for(var x = 0; x < board.cols; x++) {
		hidden = createDot(
			board.padding.left + board.x + x * board.spacing,
			board.hidden.offset.top,
			board.dot.radius, randColor(game.colorCollection), -1, true);
		hiddenCollection.push(hidden);
	}
}