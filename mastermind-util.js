function reset(ctx, game) {
	var board = game.config;

	var item;
	for(var i in dotCollection) {
		item = dotCollection[i];
		item.color = board.dot.color.default;
	}

	for(var i in hintCollection) {
		item = hintCollection[i];
		item.color = board.hint.color.default;
	}

	game.poolIndex = -1;
	game.attempts = board.rows-1;
	game.over = false;

	// reset hidden colors
	var hiddenDot;
	for(var x = 0; x < board.cols; x++) {
		hiddenDot = hiddenCollection[x];
		hiddenDot.color = randColor(game.colorCollection);
		hiddenDot.hidden = true;
	}

	render(ctx, game);
}

function rand(min, max) {
	return Math.floor(Math.random() * (max - min)) + min;
}

function randColor(collection) {
	return collection[rand(0, collection.length)];
}

function createDot(posX, posY, size, color, rowIndex, hidden) {
	var hide = hidden || false,
		dot = { x: posX, y: posY, radius: size, row: rowIndex, color: color, hidden: hide };

	return dot;
}

function createHints(posX, posY, size, color, rowIndex, offset) {
	var hints = [];

	hints.push(createDot(posX+offset.left+offset.x, posY-offset.y, size, color, rowIndex));
	hints.push(createDot(posX+offset.left+offset.x*2, posY-offset.y, size, color, rowIndex));
	hints.push(createDot(posX+offset.left+offset.x, posY+offset.y, size, color, rowIndex));
	hints.push(createDot(posX+offset.left+offset.x*2, posY+offset.y, size, color, rowIndex));

	return hints;
}

function moveCircle(index, mouseX, mouseY) {
	var draggedItem = poolCollection[index];
	draggedItem.x = mouseX;
	draggedItem.y = mouseY;
}

function setHints(game, hints) {
	var board = game.config,
		rowIndex = game.attempts;
	for(var i = 0; i < hints.length; i++) {
		hintCollection[rowIndex*board.cols+i].color = hints[i];
	}
}

function revealHidden() {
	var item;
	for(var i in hiddenCollection) {
		item = hiddenCollection[i];
		item.hidden = false;
	}
}

function hoverCircle(objects, mouseX, mouseY) {

	for(var i in objects) {
		if(isOverCircle(objects[i], mouseX, mouseY)) {
			wrapper.setAttribute('class', 'hover');
			return;
		}
	}

	// [optional] make it compatible to use multiple interaction classes
	wrapper.setAttribute('class', '');
}

function isOverCircle(circle, mouseX, mouseY) {
	// a² + b² = c²
	var maxDistance = Math.pow(circle.radius, 2),
		mouseDistance = Math.pow(mouseX - circle.x, 2) + Math.pow(mouseY - circle.y, 2);

	if(maxDistance >= mouseDistance) {
		return true;
	}

	return false;
}

function isOnCurrentRow(item, rowIndex) {
	return item.row == rowIndex;
}

function isColorSet(item, dot) {
	return item.color == dot.color;
}

function arrayContains(collection, value) {
	for(var i in collection) {
		if(collection[i] == value) {
			return true;
		}
	}

	return false;
}

function isRowFilled(collection, rowIndex, rowLength, defaultColor) {
	for(var i = 0; i < rowLength; i++) {
		if(collection[rowLength*rowIndex+i].color == defaultColor) {
			return false;
		}
	}

	return true;
}

function isGameOver(game, hints) {
	var count = 0;

	for(var i in hints) {
		if(hints[i] == "black") {
			count++;
		}
	}

	return (count == game.config.cols);
}