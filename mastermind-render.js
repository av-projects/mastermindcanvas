function clearBoard(ctx, board) {
	ctx.clearRect(0, 0, board.canvas.w, board.canvas.h);

	generateShadows(ctx, board.x, board.y, board.w, board.h, 10);

	ctx.fillStyle = board.color;
	ctx.fillRect(board.x, board.y, board.w, board.h);
}

function drawDot(ctx, dot) {
	ctx.beginPath();
	ctx.arc(dot.x, dot.y, dot.radius, 0, 360);
	ctx.closePath();
	ctx.lineWidth = 2;
	ctx.stroke();
	ctx.fill();
}

function drawTriangle(ctx, x, y, size, color) {
	size *= .5;
	var points = [[x-size, y-size], [x+size, y], [x-size, y+size]];

	fillShape(ctx, points, color);
}

function drawEndBoard(ctx, game) {
	if(!game.over) {
		return;
	}

	var board = game.config,
		hiddenBoard = board.hidden.board,
		firstHidden = hiddenCollection[0];

	generateShadows(ctx,
		firstHidden.x-hiddenBoard.padding.left, hiddenBoard.offset.top,
		hiddenBoard.w, hiddenBoard.h, 10);

	ctx.fillStyle = board.color;
	ctx.fillRect(firstHidden.x-hiddenBoard.padding.left, hiddenBoard.offset.top,
		hiddenBoard.w, hiddenBoard.h);
}

function generateShadows(ctx, x, y, w, h, depth) {
	var shadows = [
		[
			[x-depth, y-depth], [x, y],
			[x, y+h], [x-depth, y+h+depth]
		],
		[
			[x+w+depth, y-depth], [x+w, y],
			[x+w, y+h], [x+w+depth, y+h+depth],
			[x+w+depth, y-depth] // wtf js????
		]
	];

	ctx.fillStyle = "grey";
	ctx.fillRect(x-depth, y-depth, w+depth*2, h+depth*2);

	var points;
	for(var i in shadows) {
		ctx.fillStyle = "#aaaaaa";
		points = shadows[i];

		ctx.beginPath();
		ctx.moveTo(points[i][0], points[i][1]);
		for(var n = 1; n < points.length; n++) {
			ctx.lineTo(points[n][0], points[n][1]);
		}
		ctx.closePath();
		ctx.fill();
	}
}

function fillShape(ctx, points, color) {
	ctx.fillStyle = color;
	ctx.beginPath();
	var i = 0;
	ctx.moveTo(points[i][0], points[i][1]);
	for(i = 1; i < points.length; i++) {
		ctx.lineTo(points[i][0], points[i][1]);
	}
	ctx.closePath();
	ctx.fill();
}

function render(ctx, game) {
	// draw board
	clearBoard(ctx, game.config);

	drawEndBoard(ctx, game);

	// draw dots
	var item;
	for(var i in game.renderObjects) {
		item = game.renderObjects[i];

		if(item.hidden) {
			continue;
		}

		ctx.fillStyle = item.color;
		drawDot(ctx, item);
	}
}

function animationLoop(ctx, game) {
	if(game.over) {
		window.requestAnimationFrame(animationLoop.bind(this, ctx, game));
		return;
	}

	render(ctx, game); // clear screen before drawing animation

	// the offset for the triangle movement
	var movement = 2.5*Math.sin(Date.now()*0.012),
	// let the triangle appear before the first item
		firstItem = dotCollection[game.attempts*game.config.cols];
	drawTriangle(ctx, firstItem.x-30+movement, firstItem.y, 14, "black");

	window.requestAnimationFrame(animationLoop.bind(this, ctx, game));
}