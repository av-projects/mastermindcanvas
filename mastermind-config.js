// game configuration
var board = {
	canvas: { w: 400, h: 600 },
	rows: 10,
	cols: 4,
	color: "#ffffff",
	x: 75, y: 85, 
	w: 250, h: 480,
	padding: { top: 40, left: 48, bottom: 30, right: 30 },
	spacing: 37,
	dot: {
		radius: 10,
		color: { default: "#aaaaaa" }
	},
	hint: {
		color: { default: "#aaaaaa" },
		radius: 5,
		offset: { left: 20, x: 14, y: 7 }
	},
	pool: {
		spacing: 2.9,
		offset: { top: 60 }
	},
	hidden: {
		board: {
			w: 150, h: 40,
			padding: {
				left: 20
			},
			offset: { top: 20 }
		},
		offset: { top: 40 }
	}
},
colors = [
	"#ff0000", "#ff8800", "#ffdd00",
	"#33cc00", "#0000ff", "#cc00ff"
];