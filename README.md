# MasterMind
+ A small javascript game

# Rules
+ You start with a collection of colors
+ Fill in the correct order of colors to win this game. You have 10 tries.
+ The horizontal alligned fields are your playground.
+ The 4 dots on the right show you, whether you hit the right colors.
    + A black dot means correct color in the correct position.
    + A white dot means correct color, but wrong position.
+ Check your solution with the "Check" button to move on.
+ Reference: https://de.wikipedia.org/wiki/Mastermind_(Spiel)