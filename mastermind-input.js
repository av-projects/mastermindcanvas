function onMouseDown(game, e) {
	if(game.over) {
		return;
	}

	var item;
	// check if mouse is above the color pool collection
	for(var i in poolCollection) {
		item = poolCollection[i];

		if(isOverCircle(item, e.offsetX, e.offsetY)) {
			game.poolIndex = i;
			game.lastPosition = { x: item.x, y: item.y };
			break;
		}
	}

	// [optional feature] check if mouse is above an empty dot area
	if(game.poolIndex < 0) {
		for(var i in dotCollection) {
			item = dotCollection[i];

			if(isOverCircle(item, e.offsetX, e.offsetY)) {
				break;
			}
		}
	}

	e.stopPropagation();
}

function onMouseMove(ctx, game, e)
{
	hoverCircle(poolCollection, e.offsetX, e.offsetY);

	// only proceed if color has been picked from color pool
	if(game.poolIndex < 0) {
		return;
	}

	moveCircle(game.poolIndex, e.offsetX, e.offsetY);

	// render whole game while sth is in movement
	render(ctx, game);

	e.stopPropagation();
}

function onMouseUp(ctx, game, e) {
	var board = game.config;

	// reset position of the dragged item
	if(game.poolIndex >= 0) {
		var draggedItem = poolCollection[game.poolIndex];

		var item;
		for(var i in dotCollection) {
			item = dotCollection[i];
			if(!isColorSet(item, draggedItem) && 
				isOnCurrentRow(item, game.attempts) && 
				isOverCircle(item, e.offsetX, e.offsetY)) {
				
				item.color = draggedItem.color;
				break;
			}
		}

		
		draggedItem.x = game.lastPosition.x;
		draggedItem.y = game.lastPosition.y;
	}

	// reset values
	game.lastPosition = null;
	game.circleIndex = -1;
	game.poolIndex = -1;

	render(ctx, game);

	e.stopPropagation();
}

function checkBoard(ctx, game, e) {
	var board = game.config;

	if(game.over) {
		return;
	}

	if(!isRowFilled(dotCollection, game.attempts, board.cols, board.dot.color.default)) {
		alert("Some dots on your row are not colored!");
		return;
	}

	// check for same position and same color
	var hints = [], ignore = [],
		hiddenDot, coloredDot;
	for(var i = 0; i < board.cols; i++) {
		hiddenDot = hiddenCollection[i];
		coloredDot = dotCollection[board.cols*game.attempts+i];

		if(coloredDot.color == hiddenDot.color) {
			hints.push("black");
			ignore.push(i);
		}
	}

	// check for same color
	var ignoreColored = [];
	for(var i = 0; i < board.cols; i++) {
		if(arrayContains(ignore, i)) {
			continue;
		}

		hiddenDot = hiddenCollection[i];
		for(var k = 0; k < board.cols; k++) {
			if(arrayContains(ignore, k)) {
				continue;
			}

			coloredDot = dotCollection[board.cols*game.attempts+k];
			if(coloredDot.color == hiddenDot.color && !arrayContains(ignoreColored, k)) {
				hints.push("white");
				ignoreColored.push(k);
				break;
			}
		}
	}

	// show hints
	setHints(game, hints);

	// check if the combination is right
	if(isGameOver(game, hints)) {
		revealHidden();
		game.over = true;
	}

	render(ctx, game);

	if(game.over) {
		alert("Congrats! You've guessed my combination!");
	}

	if(game.attempts <= 0) {
		revealHidden();
		game.over = true;
		render(ctx, game);

		alert("Game over!");
		return;
	}

	// next round
	game.attempts--;
}